## What Is React?
1. React is a JS library developed by Facebook for building UI. 
2. The basic building block of React is "components" class.
3. React has different kinds of components but the most basic is React.Component class.
4. Each React element is a JavaScript object that you can store in a variable in the program.
5. Basically, React component are javascript classes with specific methods such as: 
> a. Contructor method which sets the state properties of the object\
> b. Render method which return the HTML code\
> c. It has props properties which are the public properties of the React class.

### Example of a React Component class or React component type
```
class ShoppingList extends React.Component {
  render() {
    return (
      <div className="shopping-list">
        <h1>Shopping List for {this.props.name}</h1>
        <ul>
          <li>Instagram</li>
          <li>WhatsApp</li>
          <li>Oculus</li>
        </ul>
      </div>
    );
  }
}
```

**Note:** A component takes in parameters, called props (short for “properties”), and returns a hierarchy of views(HTML) to display via the **render** method.

```
Example usage: 
<ShoppingList name="Mark" />
```

## What is JSX?
1. JSX provides HTML like systax inside the Javascript
2. We can evaluate any Javascript expression with {} in the JSX
3. JSX is supported in the ES6 and higher version of Javascript.

## What is React State?
1. `state` is the **private** variables of a react class.
2. React components can have **state** by setting `this.state` in their constructors which takes the **props** property of the React class.
3. `this.state` should be considered as private to a React component that it's defined in.
4. When you call `setState` in a component, React automatically updates the child components inside of it too.

## Function Component
1. Function component are the ones which only contain a *render* method and don't have their own state.
2. For function component, instead of writing them as a class, we can just define a function that takes props as input and return what is to be rendered.

## Important Points
1. `props` property of a component can also be a function (not just a value).
2. HTML elements has special properties called event listeners which then get handled by the event handler functions.
3. In React, it's conventional to use on[Event] names for props which represent events and handle[Event] for the methods which handle the events.
4. In React, **we do not mutate the data** becuase it is easy to find when the changes have been made in the immutable data so the component can re-render itself.
5. When the state of the component changes, the component re-renders itself via the render method and it means, it will also re-render the entire JSX components used inside the render method of the parent component.
6. To collect data from multiple children, or to have two child components communicate with each other, you need to declare the shared state in their parent component instead. The parent component can pass the state back down to the children by using props; this keeps the child components in sync with each other and with the parent component.